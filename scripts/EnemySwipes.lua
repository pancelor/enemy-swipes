local dev
-- dev=true

local action = require "necro.game.system.Action"
local attack = require "necro.game.character.Attack"
local commonSpell = require "necro.game.data.spell.CommonSpell"
local components = require "necro.game.data.Components"
local customEntities = require "necro.game.data.CustomEntities"
local damage = require "necro.game.system.Damage"
local ecs = require "system.game.Entities"
local event = require "necro.event.Event"
local invincibility = require "necro.game.character.Invincibility"
local move = require "necro.game.system.Move"
local object = require "necro.game.object.Object"
local spell = require "necro.game.spell.Spell"
local spellItem = require "necro.game.item.SpellItem"
local voice = require "necro.audio.Voice"

if dev then
  event.levelLoad.add("spawn", {order="entities"},function (ev)
    object.spawn("Slime2",2,0)
    object.spawn("Skeleton",-2,0)
  end)
end

-- sequence=1 means we get called _after_ the game sets the texture,
--   allowing us to re-set it
event.swipe.add("doSwipes", {key="enemy", sequence=1}, function (ev)
  if ev.attacker:hasComponent"innateAttack" then
    local dmg=ev.attacker.innateAttack.damage
    dmg=math.max(0,math.min(7,dmg))
    ev.swipe.texture = "mods/enemyswipes/images/"..dmg..".png"
  end
end)
